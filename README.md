# k8sdemositeapi

## DOCS
https://medium.com/@pavelfokin/how-to-build-a-minimal-golang-docker-image-b4a1e51b03c8  
https://laurent-bel.medium.com/running-go-on-docker-comparing-debian-vs-alpine-vs-distroless-vs-busybox-vs-scratch-18b8c835d9b8  
https://medium.com/@maheshdere/use-delve-to-debug-go-code-instead-of-print-statement-770d68563892  
https://betterprogramming.pub/path-to-a-perfect-go-dockerfile-f7fe54b5c78c  
https://www.docker.com/blog/faster-multi-platform-builds-dockerfile-cross-compilation-guide/  
https://medium.com/@leonardo5621_66451/how-to-dockerize-a-go-application-d196ea292c34
https://zaher.dev/blog/postgresql-on-k3s


## WORKFLOW
kubectl apply -f k8s/postgresql/postgres-configmap.yaml  
kubectl apply -f k8s/postgresql/local-pv.yaml  
kubectl get pvc  
kubectl apply -f k8s/postgresql/postgres-join.yaml  
kubectl delete -f k8s/postgresql/postgres-join.yaml    
kubectl describe pod postgres-7d9c89cc94-5fzdn   

kubectl get svc     
k 

kubectl exec -it pod/postgres-79d9bd959f-6rdcv -- psql -h localhost -U root --password -p 5432 appdb 
\conninfo

psql -h 144.21.36.80 -U root -W -p 30298 appdb

\conninfo

## DB
```
CREATE TABLE PASSENGERS(
"Id" INT PRIMARY KEY NOT NULL,
"Name" VARCHAR (100) NOT NULL,
"Email" VARCHAR (255) UNIQUE NOT NULL,
"Age" INTEGER NOT NULL,
"Travel_to" VARCHAR (255) NOT NULL,
"Payment" INTEGER,
"Travel_date" DATE

);

INSERT INTO "passengers" ("Id", "Name", "Email", "Age", "Travel_to", "Payment", "Travel_date")
VALUES
(2, 'Anna', 'anna@gmail.com', 19, 'NewYork', 405000, '2019-10-3'),
(3, 'Wonder', 'wonder2@yahoo.com', 32, 'Sydney', 183000, '2012-8-5'),
(4, 'Stacy', 'stacy78@hotmail.com', 28, 'Maldives', 29000, '2017-6-9'),
(5, 'Stevie', 'stevie@gmail.com', 49, 'Greece', 56700, '2021-12-12'),
(6, 'Harry', 'harry@gmail.com', 22, 'Hogwarts', 670000, '2020-1-17');
```

## DOCKER
docker buildx use $(docker buildx create --platform linux/amd64,linux/arm64)  
docker buildx build -t "remotejob/k8sdemositeapi:v1.0" -f Dockerfile.psql --platform linux/amd64,linux/arm64 --push .

## API Service
kubectl apply -f k8s/domain/cert-rollersoft-ml.yml  
kubectl apply -f k8s/api/corsheaders.yml  
kubectl apply -f k8s/api/k8sdockepsqlservice.yml  
kubectl delete -f k8s/api/k8sdockepsqlservice.yml   
kubectl rollout restart deployment k8sdemositeapi-golang 

## Tests
kubectl port-forward -n demoproject svc/k8sdemositeapi-svc 8020:8020

curl https://psqlapi.demoproject.one/api




