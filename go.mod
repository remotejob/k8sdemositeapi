module gitlab.com/remotejob/k8sdemositeapi

go 1.21

require github.com/lib/pq v1.10.9

require github.com/rs/cors v1.10.1 // indirect
