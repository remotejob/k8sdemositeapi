package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/lib/pq"
	"github.com/rs/cors"
)

const (
	// host     = "postgres.pg-dev.svc.cluster.local"
	host     = "postgres"
	port     = 5432
	user     = "root"
	password = "passwordpsql"
	dbname   = "appdb"
)

type sandbox struct {
	Name string
	Email string
	// Age   int64
	Travel_to string
	// Payment int64
}

func main() {

	mux := http.NewServeMux()

	mux.HandleFunc("/api", func(w http.ResponseWriter, r *http.Request) {
		// connection string
		psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

		// open database
		db, err := sql.Open("postgres", psqlconn)
		CheckError("Open",err)
		err = db.Ping()
		CheckError("PING",err)

		snbs := make([]sandbox, 0)


		ctr := `SELECT "Name","Email","Travel_to" FROM passengers;`

		rows, err := db.Query(ctr)
		CheckError("Query",err)

		for rows.Next() {

			snb := sandbox{}
			err := rows.Scan(&snb.Name, &snb.Email,&snb.Travel_to)
			CheckError("Scan",err)
			log.Println("snb",snb)
			snbs = append(snbs, snb)
		}
		w.Header().Set("Content-Type", "application/json")

		jsonResp, err := json.Marshal(snbs)

		w.Write(jsonResp)
	})

	handler := cors.Default().Handler(mux)

	http.ListenAndServe(":8020", handler)
}

func CheckError(msg string,err error) {
	if err != nil {

		log.Println(msg,err)
		// panic(err)
	}
}
